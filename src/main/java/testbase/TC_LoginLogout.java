package testbase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import io.qameta.allure.Description;

public class TC_LoginLogout {
	public static WebDriver driver= null;
	SoftAssert assertion= new SoftAssert();
	
//	Defining driver and url
	//@BeforeClass
	 public static WebDriver chromeBrowser() 
	 { 	
			if(driver == null){
//				
//			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
//			driver = new ChromeDriver();		
				
				System.setProperty("webdriver.gecko.driver", "geckodriver.exe");			
				driver= new FirefoxDriver();					
				String baseUrl= "http://demo.amrutsoftware.com:8080";
				driver.get(baseUrl);
			}
			return driver;
	 	}
	//login to jira 
		@Test(priority = 1)
		@Description("Valid Login Scenario with username and password")
		 public void TC1_Login()
		 {	
			driver=chromeBrowser() ;
		 	 try
		 	 {
		 		WebElement login = driver.findElement(By.id("login-form-username"));
				 login.sendKeys("rakesh");
			 Thread.sleep(2000);
			 WebElement password = driver.findElement(By.id("login-form-password"));
				password.sendKeys("Rakesh123$");
				Thread.sleep(2000);
				
				WebElement loginbtn = driver.findElement(By.name("login"));
				loginbtn.click();
				Thread.sleep(4000);
				
				assertion.assertTrue(driver.findElement(By.id("dashboard")).isDisplayed(),"Login successfully .");
				assertion.assertAll();
				//logout();
	 	 }
	 	catch(Exception e)
	 	{
	 		System.out.println(e.toString());
	 		
	 	}
	}
//	@Test(priority =3)
//	  public void M63_TC1() throws  IOException
//{	
//				String i ="3";
//				assertEquals(i, "3");
//		  		System.out.println("ACK_01");
//	  }
//	@Test(priority =4)
//	  public void M63_TC2() throws  IOException
//{	
//				String i ="3";
//				assertEquals(i, "6");
//		  		System.out.println("ACK_01");
//	  }
//	@Test(priority =5)
//	  public void M63_TC3() throws  IOException
//{	
//				String i ="3";
//				assertEquals(i, "4");
//		  		System.out.println("ACK_01");
//	  }
//	@Test(priority =6)
//	  public void M63_TC4() throws  IOException
//{	
//				String i ="3";
//				assertEquals(i, "2");
//		  		System.out.println("ACK_01");
//	  }
//	@Test(priority =7)
//	  public void M63_TC5() throws  IOException
//{	
//				String i ="3";
//				assertEquals(i, "3");
//		  		System.out.println("ACK_01");
//	  }

	@Test(priority=2)
	 public void TC2_logout() throws InterruptedException
	 {
	 	try{
//	 	Thread.sleep(2000);
	 	 WebElement admin = driver.findElement(By.xpath("//*[@id='header-details-user-fullname']/span/span/img"));
	 	Thread.sleep(2000);
	 	 admin.click();
	 	// System.out.println("Click on logout button...");
	 	driver.findElement(By.xpath("//*[@id='log_out']")).sendKeys(Keys.ENTER);
	
	 	}
	 	catch(Exception e)
	 	{
	 		System.out.println("Logout successfully...... ");
	 		
	 	}
	 	Thread.sleep(2000);
	 	
	 	driver.quit();
	}
//	@AfterClass
	public void BrowserClose()
	{
		driver.quit();
          
          
	}
	
}
